<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcsbtRev extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ccsbt_revs', function(Blueprint $table)
		{
			$table->increments("id");
			$table->string("ccsbt_vessel_id");
			$table->string("ccsbt_registration_number");
			$table->string("authorising_state_state_fishing_entity");
			$table->string("flag");
			$table->string("flag_previous");
			$table->string("vessel_name");
			$table->string("vessel_name_previous");
			$table->string("registration_number");
			$table->string("callsign");
			$table->string("date_authorisation_starts");
			$table->string("date_authorisation_ends");
			$table->string("vessel_new_to_lsfv_list");
			$table->string("length");
			$table->string("length_type");
			$table->string("tonnage");
			$table->string("previously_suspended_by_country_name");
			$table->string("vessel_type");
			$table->string("gear_type");
			$table->string("owner_name");
			$table->string("owner_address");
			$table->string("owner_country");
			$table->string("operator_name");
			$table->string("operator_address");
			$table->string("operator_country");
			$table->string("changes");
			$table->integer('ccsbt_id')->unsigned();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ccsbt_revs', function(Blueprint $table)
		{
			//
		});
	}

}
