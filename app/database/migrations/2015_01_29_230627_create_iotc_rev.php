<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIotcRev extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('iotc_revs', function(Blueprint $table)
		{
			$table->increments('id');			
			$table->string('owner_name', 100)->nullable(); 
			$table->string('owner_address', 100)->nullable(); 
			$table->string('master_name', 100)->nullable(); 
			$table->string('master_nationality', 100)->nullable(); 
			$table->string('reg_port', 100)->nullable(); 
			$table->string('built_in_country', 100)->nullable(); 
			$table->string('built_in_year', 100)->nullable(); 
			$table->string('crew', 100)->nullable(); 
			$table->string('length', 100)->nullable(); 
			$table->string('length_units', 100)->nullable(); 
			$table->string('length_type', 100)->nullable(); 
			$table->string('moulded_depth', 100)->nullable(); 
			$table->string('moulded_depth_units', 100)->nullable(); 
			$table->string('beam', 100)->nullable(); 
			$table->string('beam_units', 100)->nullable(); 
			$table->string('tonnage', 100)->nullable(); 
			$table->string('tonnage_type', 100)->nullable(); 
			$table->string('engine_power', 100)->nullable(); 
			$table->string('power_units', 100)->nullable(); 
			$table->string('freezer_types', 100)->nullable(); 
			$table->string('freezing_capacity', 100)->nullable(); 
			$table->string('freezing_capacity_units', 100)->nullable(); 
			$table->string('number_of_freezers', 100)->nullable(); 
			$table->string('fishhold_capacity', 100)->nullable(); 
			$table->string('fishhold_cap_units', 100)->nullable(); 
			$table->string('flag', 100)->nullable(); 
			$table->string('registration_number', 100)->nullable(); 
			$table->string('ircs', 100)->nullable(); 
			$table->string('vessel_type', 100)->nullable(); 
			$table->string('win', 100)->nullable(); 
			$table->string('imo-lr', 100)->nullable(); 
			$table->string('vid', 100)->nullable(); 
			$table->string('submitted_by_ccm', 100)->nullable(); 
			$table->string('attachments', 100)->nullable(); 
			$table->string('communication_details', 100)->nullable(); 
			$table->string('fishing_methods', 100)->nullable(); 
			$table->string('previous_names', 100)->nullable(); 
			$table->string('previous_flags', 100)->nullable(); 
			$table->string('under_charter', 100)->nullable(); 
			$table->string('charterer_name', 100)->nullable(); 
			$table->string('chartering_ccm', 100)->nullable(); 
			$table->string('address_of_charter', 100)->nullable(); 
			$table->string('charter_start_date', 100)->nullable(); 
			$table->string('charter_end_date', 100)->nullable(); 
			$table->string('auth_type', 100)->nullable(); 
			$table->string('auth_number', 100)->nullable(); 
			$table->string('auth_area', 100)->nullable(); 
			$table->string('auth_species', 100)->nullable(); 
			$table->string('auth_period_from', 100)->nullable(); 
			$table->string('auth_period_to', 100)->nullable(); 
			$table->string('purse_seine_vessel_authorised_to_tranship_at_sea', 100)->nullable(); 
			$table->string('authorisation_to_tranship_on_the_high_seas', 100)->nullable(); 
			$table->string("changes");
			$table->integer('iotc_id')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('iotc_revs', function(Blueprint $table)
		{
			//
		});
	}

}
