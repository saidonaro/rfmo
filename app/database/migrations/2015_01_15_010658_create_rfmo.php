<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfmo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	

		Schema::create('rfmos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('vessel_name', 200);

			$table->string('previous_name', 200);

			$table->string('rfmo_reg_number', 50);

			$table->string('lloyd_reg', 50);

			$table->string('fishing_permit_number', 50)->nullable();

			$table->string('nomor_siup', 50)->nullable();

			$table->string('periode_licency', 50)->nullable();

			$table->string('vessel_communication', 50)->nullable();
			
			$table->string('satelite_telephone_number', 50)->nullable();
			
			$table->string('port_of_registry', 50)->nullable();
			
			$table->string('flag_country', 50)->nullable();
			
			$table->string('previous_flag', 50)->nullable();
			
			$table->string('vessel_type', 50)->nullable();
			
			$table->float('moulded_depth')->nullable()->length(10)->unsigned();
			
			$table->float('gross_reg_tonnage')->nullable()->length(10)->unsigned();
			
			$table->float('gross_tonnage')->nullable()->length(10)->unsigned();
			
			$table->string('master_name', 200)->nullable();
			
			$table->string('master_nationality', 200)->nullable();
			
			$table->string('owner_name', 200)->nullable();
			
			$table->string('owner_nationality', 200)->nullable();
			
			$table->string('chartered_name', 200)->nullable();
			
			$table->string('chartered_nationality', 200)->nullable();
			
			$table->string('operator_name', 200)->nullable();
			
			$table->string('operator_nationality', 200)->nullable();
			
			$table->string('main_engine', 100)->nullable();
			
			$table->integer('boat_horse_power')->nullable()->length(10)->unsigned();
			
			$table->string('location_built_shipyard', 50)->nullable();
			
			$table->string('auxiliary_engine', 50)->nullable();
			
			$table->date('year_built_shipyard')->nullable();
			
			$table->string('material', 50)->nullable();
			
			$table->string('base_porting', 50)->nullable();
			
			$table->string('porting_come_over', 50)->nullable();
			
			$table->string('area_of_operation', 50)->nullable();
			
			$table->integer('fishing_capacity')->nullable()->length(10)->unsigned();
			
			$table->string('support_vessel', 50)->nullable();
			
			$table->string('target_species', 50)->nullable();
			
			$table->string('gear_types', 50)->nullable();
			
			$table->string('main_line', 50)->nullable();
			
			$table->string('alternative_gear', 50)->nullable();
			
			$table->string('freezer_type', 50)->nullable();

			$table->float('capacity_freezer')->nullable()->length(10)->unsigned();
			
			$table->integer('number_of_freezer')->nullable()->length(10)->unsigned();
			
			$table->float('fish_hold_capacity')->nullable()->length(10)->unsigned();
			
			$table->integer('normal_crew_complement')->nullable()->length(10)->unsigned();
			
			$table->string('vessel_photo', 200)->nullable();
			
			$table->integer('user_id')->unsigned();
			
			$table->integer('rfmo_master_id')->unsigned();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('rfmos');

	}

}
