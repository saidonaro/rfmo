<?php

class CcsbtRevController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function getRevs()
	{
		$get_filter = Input::get('filter');
		$ccsbt_id = $get_filter['filters'][0]['value'];
		// dd( $ccsbt_id);
		// $get_filter = str_replace("eq", "=", $get_filter);
		// dd($get_filter);
		$ccsbt_rev = Ccsbt_rev::where('ccsbt_id','=',$ccsbt_id)->orderBy('created_at', 'DESC')->get();
		// dd($ccsbt_rev);	
		return Response::json($ccsbt_rev , 200, array('Content-Type' => 'application/javascript')  )->setCallback(Input::get('callback'));

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$ccsbt_rev = Ccsbt_rev::find($id);

		// var_dump($ccsbt);
		return Response::json($ccsbt_rev , 200, array('Content-Type' => 'application/javascript')  )->setCallback(Input::get('callback'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
