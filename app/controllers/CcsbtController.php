<?php

class CcsbtController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /ccsbt
	 * 
	 * @return Response
	 */
	public function index()
	{
		$ccsbt = Ccsbt::all();
		return Response::json($ccsbt , 200, array('Content-Type' => 'application/javascript')  )->setCallback(Input::get('callback'));
	}

	public function getReport()
	{

		$ccsbt = Ccsbt::all();
		// $sql_newest_rev = "SELECT crev.* FROM ccsbt_revs crev
		// 					INNER JOIN 
		// 					( SELECT ccsbt_id, MAX(created_at) AS
		// 						 maxdate FROM ccsbt_revs GROUP BY ccsbt_id) group_rev
		// 					ON crev.ccsbt_id =  group_rev.ccsbt_id
		// 					AND crev.created_at = group_rev.maxdate";
		// $ccsbt_revs = Ccsbt_rev::select(
		// 								DB::raw("crev.*")
		// 									);

		// $ccsbt_revs = DB::select( DB::raw( $sql_newest_rev ) );

		// $ccsbt_revs = $get_result->toArray();

		// $ccsbt_data = array('all' => $ccsbt, 'revs' => $ccsbt_revs  );

		// var_dump($ccsbt_data);die;
		Excel::create('CCSBT Vessel Report', function($excel) use($ccsbt){

			$excel->sheet('All', function($sheet) use($ccsbt){

		        $sheet->fromModel($ccsbt, null, 'A2', true);
		        // $sheet->fromArray($ccsbt_data['all'], null, 'A2', false, false);

		    });

		    // $excel->sheet('Recently Updated', function($sheet) use($ccsbt_data){

		    //     // $sheet->fromArray($data, null, 'A1', false, false);
		    //     $sheet->fromArray($ccsbt_data['revs'], null, 'A2', false,false);

		    // });

		})->export('xlsx');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /ccsbt/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$ccsbt = new Ccsbt;

		$get_input = Input::get();

		$ccsbt = $get_input;

		$ccsbt->save();
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /ccsbt
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /ccsbt/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /ccsbt/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ccsbt = Ccsbt::find($id);

		// var_dump($ccsbt);

		return View::make('vessel.ccsbt.edit')->with('ccsbt', $ccsbt);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /ccsbt/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
			$post_data = Input::all();

			$ccsbt = Ccsbt::find($id);

			$ccsbt->fill($post_data);
			
			$check_changes =  $ccsbt->getDirty();

			if( count( $check_changes ) > 0 )
			{
				$json_changes = json_encode($check_changes);
				// var_dump(json_encode($changes));die;

	            $ccsbt->save();

				$ccsbt_rev = new Ccsbt_rev();

				$ccsbt_rev->fill($post_data);

				$ccsbt_rev->changes = $json_changes;
				$ccsbt_rev->ccsbt_id = $id;

				$ccsbt_rev->save();

	            // redirect
	            Session::flash('message', 'Successfully updated vessel '.$ccsbt->vessel_name.'!');
	            return Redirect::to('/table/ccsbt');
			}else{
				Session::flash('message', 'No changes!');
	            return Redirect::to('/table/ccsbt');
			}

			
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /ccsbt/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}