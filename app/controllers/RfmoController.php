<?php

class RfmoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /rfmo
	 *
	 * @return Response
	 */
	public function index()
	{
		$rfmo = Rfmo::all();
		return Response::json($rfmo , 200, array('Content-Type' => 'application/javascript')  )->setCallback(Input::get('callback'));
	}


	/**
	 * Show the form for creating a new resource.
	 * GET /rfmo/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /rfmo
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /rfmo/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /rfmo/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /rfmo/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /rfmo/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}