<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::resource('ccsbt', 'CcsbtController'); // index,create,store,show,edit,update,show
Route::resource('ccsbt_rev', 'CcsbtRevController'); // index,create,store,show,edit,update,show

Route::get('/rev/ccsbt', 'CcsbtRevController@getRevs'); // Param ccsbt_id passing via get
Route::get('/report/ccsbt', 'CcsbtController@getReport'); // Param ccsbt_id passing via get

Route::get('/table/ccsbt', function()
{
	return View::make('vessel.ccsbt.table');
});


Route::resource('iotc', 'IotcController'); // index,create,store,show,edit,update,show
Route::resource('iotc_rev', 'IotcRevController'); // index,create,store,show,edit,update,show

Route::get('/rev/iotc', 'IotcRevController@getRevs');
Route::get('/table/iotc', function()
{
	return View::make('vessel.iotc.table');
});

Route::resource('wcpfc', 'WcpfcController'); // index,create,store,show,edit,update,show
Route::resource('wcpfc_rev', 'WcpfcRevController'); // index,create,store,show,edit,update,show

Route::get('/rev/wcpfc', 'WcpfcRevController@getRevs');
Route::get('/table/wcpfc', function()
{
	return View::make('vessel.wcpfc.table');
});