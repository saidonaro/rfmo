<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>{{ $title }}</title>

    <!-- Bootstrap core CSS -->

    <link href="{{ URL::asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/main/navbar-fixed-top.css') }}" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  	<!-- Common Kendo UI CSS -->
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.common.min.css') }}" rel="stylesheet" />

    <!-- Default Kendo UI theme CSS -->
    <!-- <link href="{{ URL::asset('assets/kendo/styles/kendo.bootstrap.min.css') }}" rel="stylesheet"> -->
    <!-- <link href="{{ URL::asset('assets/kendo/styles/kendo.common-bootstrap.min.css') }}" rel="stylesheet"> -->
    <link href="{{ URL::asset('assets/main/kendo-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.common-material.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.material.min.css') }}" rel="stylesheet" />

    <!-- (optional) Kendo UI DataViz CSS, include only if you will use the data visualisation features -->
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.dataviz.min.css') }}" rel="stylesheet" />

    <!-- (optional) Kendo UI Mobile CSS, include only if you will use the mobile devices features -->
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.default.mobile.min.css') }}" rel="stylesheet" type="text/css" />


  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">RFMO</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Default</a></li>
            <li><a href="../navbar-static-top/">Static top</a></li>
            <li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
     @yield('content')

    </div> <!-- /container -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendor/kendo/js/kendo.all.min.js') }}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

    <!-- Custom Javascript-->
	<script type="text/javascript">
    		function path(url) {
                return /(.*\/)[^\/]*$/gi.exec(url)[1];
            }
          
            $(".container .form-widgets")
                .find("select:not([multiple])").kendoDropDownList().end()
                .find("select[multiple]").kendoMultiSelect().end()
                .find("input:not([type])").addClass("k-textbox").end()
                .find("input[type=date]").kendoDatePicker().end()
                .find("input[type=number]").kendoNumericTextBox({
                    format: "0 years"
                });
            $(".container textarea").kendoEditor({
                tools: [
                    "formatting",
                    "bold", "italic", "underline",
                    "strikethrough", "subscript", "superscript",
                    "justifyLeft", "justifyCenter", "justifyRight", "justifyFull",
                    "insertUnorderedList", "insertOrderedList", "indent", "outdent"
                ]
            });
    </script>
    <!-- Additional (Added from child blade-->
    @yield('javascript')

  </body>
<!-- </html> -->