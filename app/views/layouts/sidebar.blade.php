<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>{{ $page_title }}</title>

    <!-- Bootstrap core CSS -->

    <link href="{{ URL::asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Common Kendo UI CSS -->
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.common.min.css') }}" rel="stylesheet" />

    <!-- Default Kendo UI theme CSS -->
    <!-- <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.bootstrap.min.css') }}" rel="stylesheet"> -->
    <!-- <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.common-bootstrap.min.css') }}" rel="stylesheet"> -->
    <link href="{{ URL::asset('assets/main/kendo-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.common-material.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.material.min.css') }}" rel="stylesheet" />

    <!-- (optional) Kendo UI DataViz CSS, include only if you will use the data visualisation features -->
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.dataviz.min.css') }}" rel="stylesheet" />

    <!-- (optional) Kendo UI Mobile CSS, include only if you will use the mobile devices features -->
    <link href="{{ URL::asset('assets/vendor/kendo/styles/kendo.default.mobile.min.css') }}" rel="stylesheet" type="text/css" />


    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/main/simple-sidebar.css') }}" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- <script src="../../assets/js/ie-emulation-modes-warning.js"></script> -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    

  </head>

  <body>
    <div id="wrapper" class="toggled">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="{{ URL::to('/') }}">
                        RFMO
                    </a>
                </li>
                <li>
                    <a href="{{ URL::to('wcpfc') }}">WCPFC</a>
                </li>
                <li>
                    <a href="{{ URL::to('iotc') }}">IOTC</a>
                </li>
                <li>
                    <a href="{{ URL::to('ccsbt/table') }}">CCSBT</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
<!--                 <div class="row">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-11">
                    </div>
                </div>
 -->                <h2 class="page-header">
                        <button href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-th-list"></span></button>                        
                        
                        <small>                       
                        {{ $breadcrumbs }}
                        </small>

                        <img src="{{ $page_badge }}" height="70" style="float: right;">                        
                    </h2>

                <!-- <hr/> -->
                <div class="row">                    
                    <div id="main-content" class="col-lg-12">
                        @yield('content')
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <footer>Aplikasi Pendaftaran Kapal RFMO | <a href="http://cds.kkp.go.id/">Subdit ZEE. Kementrian Kelautan Perikanan</a>. All rights reserved.</footer>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendor/kendo/js/kendo.all.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendor/kendo/js/jszip.min.js') }}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->

    <!-- Custom Javascript-->

    <script type="text/javascript">
        
        String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        };

        var base_url = "{{ URL::to('/'); }}";
        var prev_url = "{{ URL::previous(); }}";

        var mkurl = function(path){
            return base_url+path;
        };

        var obj_to_dlist = function(obj_changes){
            var str_list = "<ul>";

            for (var key in obj_changes) {
              if (obj_changes.hasOwnProperty(key)) {
                var formated_key = key.split('_').join(' ').capitalize();
                str_list += "<li>"+formated_key + " : <strong>" + obj_changes[key]+" </strong></li>";
              }
            }

            str_list += "</ul>";
            return str_list;
        }
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
            // function path(url) {
            //     return /(.*\/)[^\/]*$/gi.exec(url)[1];
            // }
          
            $("#main-content .form-widgets")
                .find("select:not([multiple])").kendoDropDownList().end()
                .find("select[multiple]").kendoMultiSelect().end()
                .find("input:not([type])").addClass("k-textbox").end()
                .find("input[type=date]").kendoDatePicker().end()
                .find("input[type=number]").kendoNumericTextBox({
                    format: "0 number"
                });
            $("#main-content textarea").kendoEditor({
                tools: [
                    "formatting",
                    "bold", "italic", "underline",
                    "strikethrough", "subscript", "superscript",
                    "justifyLeft", "justifyCenter", "justifyRight", "justifyFull",
                    "insertUnorderedList", "insertOrderedList", "indent", "outdent"
                ]
            });
    </script>

    <!-- Additional (Added from child blade-->
    @yield('javascript')
  </body>
</html>