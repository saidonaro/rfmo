@extends('layouts.sidebar',['page_title' => 'RFMO Vessel', 
                            'breadcrumbs' => 'RFMO > Table RFMO Vessels',
                            'page_badge' => URL::asset('assets/main/logo/rfmo_logo.jpg')                           
                            ])


@section('content')
     <button id="button_new_vessel" type="button">
        <span class="k-icon"></span> New Vessel
    </button>

     <button id="button_new_report" type="button">
        <span class="k-icon"></span> New Report
    </button>

    <hr/>        
    <div id="grid"></div>           
           

@stop



@section('javascript')
<script type="text/javascript">
	 $(document).ready(function () {
                    $("#button_new_vessel").kendoButton({
                        icon: "plus",
                        click: function(e) {
                            window.open(mkurl('/RFMO/entry'),'_self');
                        }
                    });

                    $("#button_new_report").kendoButton({
                        icon: "insert-m",
                        click: function(e) {
                            alert('Under development.');
                        }
                    });

                    $("#grid").kendoGrid({
                        dataSource: {
                            transport: {
			                    read: {
			                        url: "/rfmo",
			                        dataType: "jsonp"
			                    }
		                	},
                        },
                        schema: {
                                model: {
                                    fields: {
                                        vessel_name: { type: "string" },
                                        rfmo_reg_number: { type: "string" },
                                        gross_tonnage: { type: "number" },
                                        owner_name: { type: "string" }
                                    }
                                }
                        },
                        height: 550,
                        groupable: true,
                        sortable: true,
                        pageable: false,
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    startswith: "Starts with",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        },
                        columns: [{
                            field: "vessel_name",
                            title: "Vessel Name",
                            width: 200
                        }, {
                            field: "rfmo_reg_number",
                            title: "RFMO Reg Number"
                        }, {
                            field: "gross_tonnage",
                            title: "GT"
                        },{
                            field: "owner_name",
                            title: "Owner"
                        }]
                    });
                });
</script>
@stop
