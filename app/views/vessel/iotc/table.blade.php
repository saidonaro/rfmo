@extends('layouts.sidebar',['page_title' => 'IOTC Vessel', 
                            'breadcrumbs' => 'IOTC > Table IOTC Vessels',
                            'page_badge' => URL::asset('assets/main/logo/iotc_logo.jpg')                           
                            ])


@section('content')
     <button id="button_new_vessel" type="button">
        <span class="k-icon"></span> New Vessel
    </button>

     <button id="button_new_report" type="button">
        <span class="k-icon"></span> New Report
    </button>
    <hr/>        
    <div id="grid"></div>           
           

@stop



@section('javascript')
<script type="text/javascript">
	 $(document).ready(function () {
                    $("#button_new_vessel").kendoButton({
                        icon: "plus",
                        click: function(e) {
                            window.open(mkurl('/iotc/entry'),'_self');
                        }
                    });

                    $("#button_new_report").kendoButton({
                        icon: "insert-m",
                        click: function(e) {
                            alert('Under development.');
                        }
                    });

                    $("#grid").kendoGrid({
                        dataSource: {
                            data: [
                                    { vesselName: "Vessel A", grossTonage: 20, owner: "Owner A" },
                                    { vesselName: "Vessel B", grossTonage: 33, owner: "Owner B" },
                                    { vesselName: "Vessel C", grossTonage: 23, owner: "Owner B" },
                                    { vesselName: "Vessel D", grossTonage: 43, owner: "Owner A" },
                                    { vesselName: "Vessel E", grossTonage: 53, owner: "Owner B" },
                                    { vesselName: "Vessel F", grossTonage: 73, owner: "Owner C" },
                                    { vesselName: "Vessel G", grossTonage: 83, owner: "Owner C" },
                                    { vesselName: "Vessel H", grossTonage: 103, owner: "Owner D" },
                                  ],
                            aggregate: [
                                { field: "grossTonage", aggregate: "sum" },
                                { field: "grossTonage", aggregate: "min" },
                                { field: "grossTonage", aggregate: "max" }
                            ]
                        },
                        height: 550,
                        groupable: true,
                        sortable: true,
                        pageable: false,
                        columns: [{
                            field: "vesselName",
                            title: "Vessel Name",
                            width: 200
                        }, {
                            field: "grossTonage",
                            title: "GT"
                        }, {
                            field: "owner",
                            title: "Owner Name"
                        }]
                    });
                });
</script>
@stop
