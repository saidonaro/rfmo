@extends('layouts.sidebar',['page_title' => 'WCPFC Vessel', 
                            'breadcrumbs' => ' WCPFC > New Vessel Data',
                            'page_badge' => URL::asset('assets/main/logo/wcpfc_logo.jpg')
                            ])


@section('content')
    <button id="button_cancel" type="button">
        <span class="k-icon"></span> Cancel
    </button>
    <button id="button_update" type="button">
        <span class="k-icon"></span> Update
    </button>
    <hr/>
            
            <section class="well">
<!--                 <h2 class="ra-well-title" style="background: url('https://www.wcpfc.int/sites/wcpfc.int/themes/wcpfc/images/bg-top3.jpg');">
                <img src="http://www.wcpfc.int/sites/wcpfc.int/themes/wcpfc/images/logo.png"/>
                </h2> -->

                <div class="form-horizontal form-widgets col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="name">Vessel name</label>
                        <div class="col-sm-8 col-md-6">
                            <input id="vessel_name" value="WCPFC VESSEL" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="birthday">Date</label>
                        <div class="col-sm-8 col-md-6">
                            <input type="date" value="10/09/1979" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="gender">Drop down</label>
                        <div class="col-sm-8 col-md-6">
                            <select id="gender">
                                <option selected>Option A</option>
                                <option>Option B</option>
                            </select>
                        </div>
                    </div>
                    
                </div>

                <div class="form-horizontal form-widgets col-sm-6">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="occupation">Input Text</label>
                        <div class="col-sm-8 col-md-6">
                            <input id="occupation" placeholder="e.g. Fulan" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="skills">Multiple Selections</label>
                        <div class="col-sm-8 col-md-6">
                            <select id="skills" multiple>
                                <option>Multi A</option>
                                <option selected>Multi B</option>
                                <option>Multi C</option>
                                <option selected>Multi D</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-4" for="experience">Number</label>
                        <div class="col-sm-8 col-md-6">
                            <input id="experience" type="number" value="4" />
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="form-horizontal form-widgets col-sm-12">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="bio">Textarea</label>
                        <div class="col-sm-10">
                            <textarea id="bio"></textarea>
                        </div>
                    </div>
                </div>                
                <div class="clearfix"></div>
            </section>

           

@stop



@section('javascript')
<script type="text/javascript">
	$(document).ready(function () {
                    $("#button_cancel").kendoButton({
                        icon: "cancel",
                        click: function(e) {
                            window.open(prev_url,'_self');
                        }
                    });

                    $("#button_update").kendoButton({
                        icon: "tick",
                        click: function(e) {
                            window.open(mkurl('/ccsbt/entry'),'_self');
                        }
                    });
                   
    });
</script>
@stop
