@extends('layouts.sidebar',['page_title' => 'CCSBT Vessel', 
                            'breadcrumbs' => 'CCSBT > Edit Vessel : '.$ccsbt->vessel_name,
                            'page_badge' => URL::asset('assets/main/logo/ccsbt_logo.jpg')                           
                            ])


@section('content')
            <section class="well">
                {{ Form::model($ccsbt, array('route' => array('ccsbt.update', $ccsbt->id), 'method' => 'PUT')) }}

                <div class="form-horizontal form-widgets col-sm-6">
                    <div class="form-group">
                        {{ Form::label('vessel_name','Vessel Name :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('vessel_name', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'vessel_name_previous','Vessel Name Previous :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('vessel_name_previous', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('ccsbt_registration_number','CCSBT Registration Number :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('ccsbt_registration_number', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('ccsbt_vessel_id','CCSBT Vessel ID :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('ccsbt_vessel_id', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'registration_number','Registration Number :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('registration_number', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'authorising_state_state_fishing_entity','Authorising State/State Fishing Entity :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('authorising_state_state_fishing_entity', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'flag','Flag :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('flag', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'flag_previous','Flag Previous :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('flag_previous', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'vessel_new_to_lsfv_list','Vessel New To LSFV List :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('vessel_new_to_lsfv_list', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'callsign','Callsign :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('callsign', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'date_authorisation_starts','Date Auth. Starts :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('date_authorisation_starts', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'date_authorisation_ends','Date Auth. Ends :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('date_authorisation_ends', null, array('class' => 'form-control')) }}
                        </div>
                    </div>                    
                    
                </div>

                <div class="form-horizontal form-widgets col-sm-6">
                    <div class="form-group">
                        {{ Form::label( 'length','Length :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('length', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'length_type','Length Type :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('length_type', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'tonnage','Tonnage :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('tonnage', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'previously_suspended_by_country_name','Previously Suspended by Country Name :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('previously_suspended_by_country_name', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'vessel_type','Vessel Type :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('vessel_type', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'gear_type','Gear Type :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('gear_type', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'owner_name','Owner Name :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('owner_name', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'owner_address','Owner Address :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('owner_address', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'owner_country','Owner Country :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('owner_country', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'operator_name','Operator Name :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('operator_name', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'operator_address','Operator Address :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('operator_address', null, array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'operator_country', 'Operator Country :', array('class' => 'control-label col-lg-4') ) }}
                        <div class="col-lg-8">                        
                        {{ Form::text('operator_country', null, array('class' => 'form-control')) }}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="buttons-wrap">
                    <button id="button_cancel" type="button" class="k-button k-state-default">
                        <span class="k-icon"></span> Cancel
                    </button>
                    <button id="button_update" type="submit" class="k-button k-state-default">
                        <span class="k-icon"></span> Update
                    </button>
                </div>
            {{ Form::close() }}
            </section>

           

@stop



@section('javascript')
<script type="text/javascript">
	$(document).ready(function () {
                    $("#button_cancel").kendoButton({
                        icon: "cancel",
                        click: function(e) {
                            window.open(prev_url,'_self');
                        }
                    });

                    $("#button_update").kendoButton({
                        icon: "tick"
                    });
    });
</script>
@stop
