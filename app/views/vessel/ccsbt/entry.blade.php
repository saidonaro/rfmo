@extends('layouts.sidebar',['page_title' => 'CCSBT Vessel', 
                            'breadcrumbs' => 'CCSBT > New Vessel Data',
                            'page_badge' => URL::asset('assets/main/logo/ccsbt_logo.jpg')                           
                            ])


@section('content')
            <section class="well">
                {{ Form::open(array('url' => 'ccsbt')) }}

                <div class="form-horizontal form-widgets col-sm-6">
                    <div class="form-group">
                        {{ Form::label('vessel_name','Vessel Name', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('vessel_name', Input::old('vessel_name'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'vessel_name_previous','Vessel Name Previous', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('vessel_name_previous', Input::old('vessel_name_previous'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('ccsbt_registration_number','CCSBT Registration Number', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('ccsbt_registration_number', Input::old('ccsbt_registration_number'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'registration_number','Registration Number', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('registration_number', Input::old('registration_number'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'authorising_state_state_fishing_entity','Authorising State/State Fishing Entity', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('authorising_state_state_fishing_entity', Input::old('authorising_state_state_fishing_entity'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'flag','Flag', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('flag', Input::old('flag'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'flag_previous','Flag Previous', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('flag_previous', Input::old('flag_previous'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'vessel_new_to_lsfv_list','Vessel New To LSFV List', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('vessel_new_to_lsfv_list', Input::old('vessel_new_to_lsfv_list'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'callsign','Callsign', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('callsign', Input::old('callsign'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'date_authorisation_starts','Date Auth. Starts', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('date_authorisation_starts', Input::old('date_authorisation_starts'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'date_authorisation_ends','Date Auth. Ends', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('date_authorisation_ends', Input::old('date_authorisation_ends'), array('class' => 'form-control')) }}
                        </div>
                    </div>                    
                    
                </div>

                <div class="form-horizontal form-widgets col-sm-6">
                    <div class="form-group">
                        {{ Form::label( 'length','Length', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('length', Input::old('length'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'length_type','Length Type', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('length_type', Input::old('length_type'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'tonnage','Tonnage', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('tonnage', Input::old('tonnage'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'previously_suspended_by_country_name','Previously Suspended by Country Name', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('previously_suspended_by_country_name', Input::old('previously_suspended_by_country_name'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'vessel_type','Vessel Type', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('vessel_type', Input::old('vessel_type'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'gear_type','Gear Type', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('gear_type', Input::old('gear_type'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'owner_name','Owner Name', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('owner_name', Input::old('owner_name'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'owner_address','Owner Address', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('owner_address', Input::old('owner_address'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'owner_country','Owner Country', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('owner_country', Input::old('owner_country'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'operator_name','Operator Name', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('operator_name', Input::old('operator_name'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'operator_address','Operator Address', array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('operator_address', Input::old('operator_address'), array('class' => 'form-control')) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label( 'operator_country', 'Operator Country',array('class' => 'control-label') ) }}
                        <div class="col-lg-10">                        
                        {{ Form::text('operator_country', Input::old('operator_country'), array('class' => 'form-control')) }}
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="buttons-wrap">
                    <button id="button_cancel" type="button" class="k-button k-state-default">
                        <span class="k-icon"></span> Cancel
                    </button>
                    <button id="button_update" type="submit" class="k-button k-state-default">
                        <span class="k-icon"></span> Update
                    </button>
                </div>
            {{ Form::close() }}
            </section>

           

@stop



@section('javascript')
<script type="text/javascript">
	$(document).ready(function () {
                    $("#button_cancel").kendoButton({
                        icon: "cancel",
                        click: function(e) {
                            window.open(prev_url,'_self');
                        }
                    });

                    $("#button_update").kendoButton({
                        icon: "tick"
                    });
    });
</script>
@stop
