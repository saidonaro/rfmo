@extends('layouts.sidebar',['page_title' => 'CCSBT Vessel', 
                            'breadcrumbs' => 'CCSBT > Table CCSBT Vessels',
                            'page_badge' => URL::asset('assets/main/logo/ccsbt_logo.jpg')                           
                            ])


@section('content')
    <span id="popupNotification"></span>

     <button id="button_new_vessel" type="button">
        <span class="k-icon"></span> New Vessel
    </button>

     <button id="button_new_report" type="button">
        <span class="k-icon"></span> New Report
    </button>

    <button id="button_sync_vessel" type="button">
        <span class="k-icon"></span> Sync from CCSBT Vessel Database
    </button>
    <hr/>        
    <div id="grid"></div>           
    <div id="details"></div>
    <div id="rev_details"></div>

    <script type="text/x-kendo-template" id="template">
                <div id="details-container">
                    <h2>#= vessel_name # #= ccsbt_registration_number #</h2>
                    <em>#= owner_name #</em>
                    <dl>
                        <dt>Auth Starts: #= kendo.toString(date_authorisation_starts, "MM/dd/yyyy") #</dt>
                        <dt>Auth Ends: #= kendo.toString(date_authorisation_ends, "MM/dd/yyyy") #</dt>
                    </dl>
                </div>
    </script>

    <script type="text/x-kendo-template" id="rev_template">
                <div id="details-container">
                    <h2>#= vessel_name # #= ccsbt_registration_number #</h2>
                    <em>#= owner_name #</em>
                    <dl>
                        <dt>Auth Starts: #= kendo.toString(date_authorisation_starts, "MM/dd/yyyy") #</dt>
                        <dt>Auth Ends: #= kendo.toString(date_authorisation_ends, "MM/dd/yyyy") #</dt>
                    </dl>
                    <p> Revision </p>
                    #= changes #                   
                </div>
    </script>

@stop



@section('javascript')`
<script type="text/javascript">
var wnd, wnd_rev,detailsTemplate,detailsTemplate_rev;

$(document).ready(function () {

// var saveToExcel = function( rows ){

// }
                    var popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
                        
                    @if (Session::has('message'))
                        popupNotification.show("{{ Session::get('message') }}", "success");
                    @endif
                  
                    $("#button_new_vessel").kendoButton({
                        icon: "plus",
                        click: function(e) {
                            window.open(mkurl('/ccsbt/entry'),'_self');
                        }
                    });

                    $("#button_sync_vessel").kendoButton({
                        icon: "refresh",
                        click: function(e) {
                            window.open(mkurl('/ccsbt/sync'),'_self');
                        }
                    });

                    $("#button_new_report").kendoButton({
                        icon: "insert-m",
                        click: function(e) {
                            window.open(mkurl('/report/ccsbt'),'_blank');                            
                        }
                    });

                    $("#grid").kendoGrid({
                        dataSource: {
                            transport: {
                                read: {
                                    url: "/ccsbt",
                                    dataType: "jsonp"
                                }
                            },
                        },
                        schema: {
                                model: {
                                    fields: {
                                        vessel_name: { type: "string" },
                                        ccsbt_registration_number: { type: "string" },
                                        tonnage: { type: "number" },
                                        owner_name: { type: "string" },
                                        callsign: { type: "string" },
                                        date_authorisation_starts: { type: "datetime" },
                                        date_authorisation_ends: { type: "datetime" },
                                        length: { type: "string" },
                                        gear_type: { type: "string" },
                                        updated_at: {type: "datetime"},
                                        created_at: {type: "datetime"}
                                    }
                                }
                        },
                        height: 550,
                        groupable: true,
                        sortable: true,
                        pageable: false,
                        // selectable: true,
                        detailInit: detailInit,
                        dataBound: function() {
                            this.expandRow(this.tbody.find("tr.k-master-row").first());
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    startswith: "Starts with",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        },
                        columns: [{
                            field: "vessel_name", title: "Vessel Name",width: 200
                        },{ 
                            command: { text: "View", click: showDetails },
                            title: " ", width: "100px" 
                        },{ 
                            command: { text: "Edit", click: editDetails },
                            title: " ", width: "100px" 
                        },{
                            field: "ccsbt_registration_number",title: "CCSBT Reg Number",width: 200
                        }, {
                            field: "tonnage",title: "GT",width: 100
                        },{
                            field: "owner_name",title: "Owner Name",width: 200
                        },{
                            field: "callsign",title: "Callsign",width: 200
                        },{
                            field: "date_authorisation_starts",title: "Date Auth. Starts",width: 200
                        },{
                            field: "date_authorisation_ends",title: "Date Auth. Ends",width: 200
                        }]
                    });
    wnd = $("#details").kendoWindow({
                            title: "Vessel Details",
                            modal: true,
                            visible: false,
                            resizable: true,
                            width: 500
                        }).data("kendoWindow");
    wnd_rev = $("#rev_details").kendoWindow({
                            title: "Revision Details",
                            modal: true,
                            visible: false,
                            resizable: true,
                            width: 500,
                            actions: ["Pin", "Refresh", "Maximize", "Close"]
                        }).data("kendoWindow");
    detailsTemplate = kendo.template($("#template").html());   
    detailsTemplate_rev = kendo.template($("#rev_template").html());   
});

function showDetails(e) {
                    e.preventDefault();
                    
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    // wnd.refresh();
                    wnd.content(detailsTemplate(dataItem));
                    wnd.center().open();
                };

function editDetails(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    window.open(mkurl('/ccsbt/'+dataItem.id)+'/edit','_self');
                };


function revChanges(e) {
                    e.preventDefault();

                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    
                    var changes = dataItem.changes.length > 0 ? obj_to_dlist( JSON.parse(dataItem.changes) ) : "-";
                    dataItem.changes =  changes;
                    wnd_rev.refresh();
                    
                    wnd_rev.content(detailsTemplate_rev(dataItem));
                    wnd_rev.center().open();
                };

function detailInit(e) {
                    $("<div/>").appendTo(e.detailCell).kendoGrid({
                        dataSource: {
                            transport: {
                                read: {
                                    url: "/rev/ccsbt",
                                    dataType: "jsonp"
                                }
                            },
                            serverFiltering: true,
                            filter: { field: "ccsbt_id", operator: "eq", value: e.data.id }
                        },
                        schema: {
                                model: {
                                    fields: {
                                        vessel_name: { type: "string" },
                                        ccsbt_registration_number: { type: "string" },
                                        tonnage: { type: "number" },
                                        owner_name: { type: "string" },
                                        callsign: { type: "string" },
                                        date_authorisation_starts: { type: "datetime" },
                                        date_authorisation_ends: { type: "datetime" },
                                        length: { type: "string" },
                                        gear_type: { type: "string" },
                                        updated_at: {type: "datetime"},
                                        created_at: {type: "datetime"},
                                        changes: {type: "string"}
                                    }
                                }
                        },
                        scrollable: false,
                        sortable: true,
                        pageable: true,
                        columns: [
                            { 
                                command: { text: "Changes", click: revChanges },
                                title: "Changes ", width: "120px" 
                            },{
                                field: "created_at", title: "Tgl Perubahan",width: 200
                            },{
                                field: "vessel_name", title: "Vessel Name",width: 200
                            },{
                                field: "owner_name",title: "Owner Name",width: 200
                            },{
                                field: "date_authorisation_starts",title: "Date Auth. Starts",width: 200
                            },{
                                field: "date_authorisation_ends",title: "Date Auth. Ends",width: 200
                            }]
                    });
                };
</script>
@stop
