@extends('layouts.sidebar',['page_title' => 'RFMO', 
                            'breadcrumbs' => ' Dashboard',
                            'page_badge' => URL::asset('assets/main/logo/rfmo_logo.jpg')                                                       
                            ])


@section('content')
            
           <div id="rfmo_grid"></div>     
            

@stop



@section('javascript')
<script type="text/javascript">
	$(document).ready(function () {
		$("#rfmo_grid").kendoGrid({
                        dataSource: {
                            transport: {
			                    read: {
			                        url: "/rfmo",
			                        dataType: "jsonp"
			                    }
		                	},
                        },
                        schema: {
                                model: {
                                    fields: {
                                        vessel_name: { type: "string" },
                                        rfmo_reg_number: { type: "string" },
                                        gross_tonnage: { type: "number" },
                                        owner_name: { type: "string" }
                                    }
                                }
                        },
                        groupable: true,
                        sortable: true,
                        pageable: false,
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    startswith: "Starts with",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        },
                        columns: [{
                            field: "vessel_name",
                            title: "Vessel Name",
                            width: 200
                        }, {
                            field: "rfmo_reg_number",
                            title: "RFMO Reg Number"
                        }, {
                            field: "owner_name",
                            title: "Owner"
                        }]
                    });
	} );
</script>
@stop
